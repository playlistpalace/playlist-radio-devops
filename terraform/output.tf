output "playlist-radio-private" {
  value = "${aws_instance.playlist-radio.*.private_ip}"
}

output "playlist-radio-public" {
  value = ["${aws_instance.playlist-radio.*.public_ip}"]
}
