resource "aws_s3_bucket" "playlist-radio-coverart" {
  bucket = "playlist-radio-coverart"
}

resource "aws_s3_bucket_policy" "playlist-radio-coverart" {
  bucket = "${aws_s3_bucket.playlist-radio-coverart.id}"
  policy =<<POLICY
{
    "Version": "2012-10-17",
    "Statement": [
        {
            "Sid": "AddPerm",
            "Effect": "Allow",
            "Principal": "*",
            "Action": "s3:GetObject",
            "Resource": "arn:aws:s3:::playlist-radio-coverart/*"
        }
    ]
}     
POLICY
}
