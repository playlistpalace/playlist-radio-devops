resource "aws_instance" "playlist-radio" {
  ami           = "ami-e81b4c90"
  instance_type = "m4.large"
  key_name = "playlistradiodev"
  vpc_security_group_ids   = ["${aws_security_group.playlist-radio-prod-SG.id}"]

  tags {
    Name = "playlist-radio"
  }
}
