resource "aws_s3_bucket" "playlist-radio-userprofilepic" {
  bucket = "playlist-radio-userprofilepic"
}

resource "aws_s3_bucket_policy" "playlist-radio-userprofilepic" {
  bucket = "${aws_s3_bucket.playlist-radio-userprofilepic.id}"
  policy =<<POLICY
{
    "Version": "2012-10-17",
    "Statement": [
        {
            "Sid": "AddPerm",
            "Effect": "Allow",
            "Principal": "*",
            "Action": "s3:GetObject",
            "Resource": "arn:aws:s3:::playlist-radio-userprofilepic/*"
        }
    ]
}     
POLICY
}
