#!/bin/bash
mv target/PlaylistPalace-0.0.1-SNAPSHOT.war ROOT.war
scp -oStrictHostKeyChecking=no -i ../../playlistdev.pem ROOT.war centos@`terraform output playlist-private`:/tmp
ssh -oStrictHostKeyChecking=no -i ../../playlistdev.pem centos@`terraform output playlist-private` "sudo mv /opt/tomcat/webapps/ROOT.war /opt/backup/ROOT$(date +%F_%R).war"
ssh -oStrictHostKeyChecking=no -i ../../playlistdev.pem centos@`terraform output playlist-private` "sudo mv /tmp/ROOT.war /opt/tomcat/webapps"
ssh -oStrictHostKeyChecking=no -i ../../playlistdev.pem centos@`terraform output playlist-private` "sudo systemctl restart tomcat.service"
ssh -oStrictHostKeyChecking=no -i ../../playlistdev.pem centos@`terraform output playlist-private` "sudo rm -rf/tmp/ credentials playlistInit.sql dbrestore.sh"
