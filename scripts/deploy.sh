#!/bin/bash
set -e
sed -i 's/prod/dev/g' /var/lib/jenkins/workspace/Prod-Playlist/src/main/resources/application.properties
sed -i "s/127.0.0.1:8080/`terraform output playlist-public`:8080/g" /var/lib/jenkins/workspace/Prod-Playlist/src/main/resources/application-dev.properties
scp -oStrictHostKeyChecking=no -i /var/lib/jenkins/playlistdev.pem -r /var/lib/jenkins/workspace/Prod-Playlist/src/main/resources/  centos@`terraform output playlist-private`:/tmp
scp -oStrictHostKeyChecking=no -i /var/lib/jenkins/playlistdev.pem /root/deployment/previousvalue  centos@`terraform output playlist-private`:/tmp
scp -oStrictHostKeyChecking=no -i /var/lib/jenkins/playlistdev.pem /root/deployment/dbdeploy.sh  centos@`terraform output playlist-private`:/tmp
ssh -oStrictHostKeyChecking=no -i /var/lib/jenkins/playlistdev.pem centos@`terraform output playlist-private` "sudo sh -x /tmp/dbdeploy.sh"
