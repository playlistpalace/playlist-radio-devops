#!/bin/bash
set -e
cd ../terraform/
scp -oStrictHostKeyChecking=no -i /var/lib/jenkins/playlistdev.pem ../scripts/dbschemarestore.sh  centos@`cat ../terraform/private`:/tmp
scp -oStrictHostKeyChecking=no -i /var/lib/jenkins/playlistdev.pem -r ../../src/main/resources  centos@`cat ../terraform/private`:/tmp
ssh -oStrictHostKeyChecking=no -i /var/lib/jenkins/playlistdev.pem centos@`cat ../terraform/private` "sudo sh -x /tmp/dbschemarestore.sh"
ssh -oStrictHostKeyChecking=no -i /var/lib/jenkins/playlistdev.pem centos@`cat ../terraform/private` "sudo rm -rf /tmp/dbschemarestore.sh"

