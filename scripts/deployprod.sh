#!/bin/bash
set -e
cd ../terraform/
scp -oStrictHostKeyChecking=no -i /var/lib/jenkins/playlistdev.pem ../scripts/dbdatarestore.sh  centos@`cat ../terraform/private`:/tmp
scp -oStrictHostKeyChecking=no -i /var/lib/jenkins/playlistdev.pem ../config/credentials  centos@`cat ../terraform/private`:/tmp
scp -oStrictHostKeyChecking=no -i /var/lib/jenkins/playlistdev.pem ../config/playlistInit.sql  centos@`cat ../terraform/private`:/tmp
ssh -oStrictHostKeyChecking=no -i /var/lib/jenkins/playlistdev.pem centos@`cat ../terraform/private` "sudo sh -x /tmp/dbdatarestore.sh"
ssh -oStrictHostKeyChecking=no -i /var/lib/jenkins/playlistdev.pem centos@`cat ../terraform/private` "sudo rm -rf /tmp/credentials /tmp/dbdatarestore.sh /tmp/playlistInit.sql"

